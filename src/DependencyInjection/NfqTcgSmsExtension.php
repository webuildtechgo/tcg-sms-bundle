<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TcgSmsBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * Class NfqTcgSmsExtension
 * @package Nfq\Bundle\TcgSmsBundle\DependencyInjection
 */
class NfqTcgSmsExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');

        $config = $this->processConfiguration(new Configuration(), $configs);

        $httpClientdef = $container->getDefinition('nfq_tcg_sms.http_client');
        $httpClientdef->replaceArgument(0, $config['base_url']);

        $tcgClientDef = $container->getDefinition('nfq_tcg_sms.tcg_client');
        $tcgClientDef->replaceArgument(2, $config['username']);
        $tcgClientDef->replaceArgument(3, $config['api_key']);
        $tcgClientDef->replaceArgument(4, $config['sender_name']);
    }
}
