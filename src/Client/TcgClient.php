<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TcgSmsBundle\Client;

use Nfq\Bundle\TcgSmsBundle\Constants\Endpoints;
use Psr\Log\LoggerInterface;

/**
 * Class TcgClient
 * @package Nfq\Bundle\TcgSmsBundle\Client
 */
class TcgClient
{
    const LOG_PREFIX = 'TCG client. ';

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var string
     */
    private $senderName;

    /**
     * @var HttpClientInterface
     */
    private $httpClient;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * TcgClient constructor.
     * @param LoggerInterface $logger
     * @param string          $username
     * @param string          $apiKey
     * @param string          $senderName
     */

    /**
     * TcgClient constructor.
     * @param LoggerInterface     $logger
     * @param HttpClientInterface $httpClient
     * @param string              $username
     * @param string              $apiKey
     * @param string              $senderName
     */
    public function __construct(
        LoggerInterface $logger,
        HttpClientInterface $httpClient,
        $username,
        $apiKey,
        $senderName
    ) {
        $this->username = $username;
        $this->apiKey = $apiKey;
        $this->senderName = $senderName;
        $this->logger = $logger;
        $this->httpClient = $httpClient;
    }

    /**
     * Sends sms.
     *
     * @param string $phone
     * @param string $message
     * @return bool
     * @throws \Exception
     */
    public function sendSms($phone, $message)
    {
        try {
            $phone = ltrim($phone, '+');
            $message = strip_tags($message);

            $params = [
                'login' => $this->username,
                'phone' => $phone,
                'text' => $message,
                'sender' => $this->senderName,
                'timestamp' => $this->getTimestamp(),
            ];

            $params['signature'] = $this->getSignature($params);

            $url = $this->getRequestURI(Endpoints::ENDPOINT_SEND, $params);

            $this->logger->debug(
                self::LOG_PREFIX . 'Trying to send sms',
                [
                    'url' => $url,
                    'phone' => $phone,
                ]
            );

            $request = $this->httpClient->get($url);
            $response = $request->send();

            if (!$response->isSuccessful()) {
                throw new \Exception(sprintf('Cannot request %s', $url));
            }

            $responseBody = $response->getBody(true);
            $responseBodyDecoded = json_decode($responseBody, true);

            if (isset($responseBodyDecoded[$phone])
                && isset($responseBodyDecoded[$phone]['error'])
                && 0 === (int)$responseBodyDecoded[$phone]['error']
            ) {
                $this->logger->info(
                    self::LOG_PREFIX . 'Message was sent successful.',
                    [
                        'response' => $responseBody,
                        'phone' => $phone,
                        'smsMessage' => $message,
                    ]
                );

                return true;
            }

            throw new \Exception(sprintf('Error while sending sms. Response: %s', $responseBody));
        } catch (\Exception $e) {
            $this->logger->error(
                self::LOG_PREFIX . 'Exception on sending sms.',
                [
                    'message' => $e->getMessage(),
                    'phone' => $phone,
                    'smsMessage' => $message,
                ]
            );

            throw $e;
        }
    }

    /**
     * @return string
     * @throws \Exception
     */
    private function getTimestamp()
    {
        $url = $this->getRequestURI(Endpoints::ENDPOINT_TIMESTAMP);

        $this->logger->debug(
            self::LOG_PREFIX . 'Trying to get timestamp',
            [
                'url' => $url,
            ]
        );

        $request = $this->httpClient->get($url);
        $response = $request->send();

        if (!$response->isSuccessful()) {
            throw new \Exception(sprintf('Cannot request %s', $url));
        }

        return $response->getBody(true);
    }

    /**
     * @param array $params
     * @return string
     */
    private function getSignature($params = [])
    {
        ksort($params);
        reset($params);

        return md5(implode($params) . $this->apiKey);
    }

    /**
     * @param string $endpoint
     * @param array  $params
     * @return string
     */
    private function getRequestURI($endpoint, $params = [])
    {
        $url = $this->httpClient->getBaseUrl() . $endpoint;

        if (!empty($params)) {
            $url .= '?' . http_build_query($params);
        }

        return $url;
    }
}
