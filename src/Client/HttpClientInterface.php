<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TcgSmsBundle\Client;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriInterface;
use Psr\Http\Message\MessageInterface;

/**
 * Class HttpClientInterface
 * @package Nfq\Bundle\TcgSmsBundle\Client
 */
interface HttpClientInterface
{
    /**
     * Return base url.
     *
     * @return string
     */
    public function getBaseUrl($url);

    /**
     * Prepare and return message.
     *
     * @return MessageInterface
     */
    public function get($uri = null, $headers = null, $options = array());

    /**
     * Send an HTTP request.
     *
     * @param RequestInterface|array $requests
     * @return ResponseInterface
     */
    public function send($requests);

    /**
     * Get a client configuration option.
     *
     * These options include default request options of the client, a "handler"
     * (if utilized by the concrete client), and a "base_uri" if utilized by
     * the concrete client.
     *
     * @param string|null $option The config option to retrieve.
     *
     * @return mixed
     */
    public function getConfig($option = null);
}
