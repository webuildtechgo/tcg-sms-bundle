# NfqTcgSmsBundle

Sends sms through TCG provider

## Installation

Define additional repository in composer.json:

```
 "repositories": [
    "nfq-packagist": {
        "type": "composer",
        "url": "https://packagist.nfq.lt"
    }
 ]
 
```

and update composer.json:

```
"require": {
    "nfq/tcg-sms-bundle": "dev-master"
},

```

Enable bundle in AppKernel.php:

```
$bundles = [
    ...
    new Nfq\Bundle\TcgSmsBundle\NfqTcgSmsBundle(),
    ...
];

```

### Configuration

First, you have to configure bundle. For the config settings.

```yaml
# app/config/config.yml

nfq_tcg_sms:
    base_url: '%tcg_sms_base_url%'
    username: '%tcg_sms_username%'
    api_key: '%tcg_sms_api_key%'
    sender_name: '%tcg_sms_sender_name%'

```

```yaml
# app/config/parameters.yml

    base_url: https://sms4.tcg.lt/external
    tcg_sms_username: testuser
    tcg_sms_api_key: 1223
    tcg_sms_sender_name: Test company
    
```

### Usage

```php

$container->get('nfq_tcg_sms.tcg_client')->sendSms($phoneNumber, $message);

```
